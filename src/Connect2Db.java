import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect2Db {
	public static void main(String[] args) {
		
		String url = "jdbc:mysql://localhost:3306/jdbc1";
		String user = "root";
		String password = "password";
		
		try {
			DriverManager.getConnection(url, user, password);
			
			System.out.println("Connection established...");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
