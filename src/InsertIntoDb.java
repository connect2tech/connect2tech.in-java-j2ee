import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertIntoDb {
	public static void main(String[] args) {
		
		String url = "jdbc:mysql://localhost:3306/jdbc1";
		String user = "root";
		String password = "password";
		String insert = "insert into Employee(id, name, department, salary) values (100,'Mike', 'Java',100)";
		
		try {
			Connection conn = DriverManager.getConnection(url, user, password);
			
			Statement stmt = conn.createStatement();
			stmt.execute(insert);
			
			System.out.println("insertion done...");
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
