import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FetechDataFromDb {
	public static void main(String[] args) {
		
		String url = "jdbc:mysql://localhost:3306/jdbc1";
		String user = "root";
		String password = "password";
		String select = "select * from Employee where salary>300";
		
		try {
			Connection conn = DriverManager.getConnection(url, user, password);
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(select);
			
			while(rs.next()){
				int id = rs.getInt("ID");
				String name = rs.getString("name");
				String department = rs.getString("department");
				int salary = rs.getInt("salary");
				
				System.out.println(id);
				System.out.println(name);
				System.out.println(department);
				System.out.println(salary);
				System.out.println("-----------------------------");
				
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
